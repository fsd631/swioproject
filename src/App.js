import React from 'react';
import './App.css';

import Portion1 from './Components/Portion1';
import Portion2 from './Components/Portion2';
import Portion3 from './Components/Portion3';
import Portion4 from './Components/Portion4';



// import Portion2 from './Components/Portion3';
// import Portion2 from './Components/Portion4';
function App() {
  return (
    <div className="App">
     <Portion1 />
     <Portion2 />
     <Portion3 />
     <Portion4 />
    </div>

  );
}

export default App;
