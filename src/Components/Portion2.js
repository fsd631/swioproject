import React from 'react'

function Portion2() {
  return (
    <div>
      <div className='container1'>
      <h1>Themes. Unlimited themes.</h1>
        <p>Themes in Sleeve make creating and switching between customizations easy. 
        your own creations with friends and install as many themes as you like with just a double-click.</p>
        <img  src="./images/pic.png" alt="Default" className="image" />
      </div>
          
      <div className='divcontainer'>
        <div id='flexitem'>
          
        <div className='div1'>
        <div id='div1'>
        <h2>Change it up</h2>
        <p>Switch between themes with just a click.
        Modify the built-in themes or create your own using Sleeve’s extensive preferences.</p>
        <img src="./images/pic2.png" alt="Default" className="image" />
        </div>
        </div>

       <div className='div1'>
       <div id='div2'>
       <h2>Shareable</h2>
        <p>Export your themes and share them with friends using the handy new Sleeve Theme file format.
        Install themes from anywhere with a double-click or a drag and drop..</p>
        <img src="./images/pic3.png" alt="Default" className="image" />
       </div>
        </div>

        </div>

      </div>
    </div>
    
  )
}

export default Portion2