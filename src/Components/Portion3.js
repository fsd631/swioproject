import React from 'react'

function Portion3() {
  return (
    <div className='final'>
        <div className='portion3'>
          <div>
          <h1>Countless ways to customize.</h1>
          <p>Customization is at the core of the Sleeve experience — choose from any combination
          of design choices, behaviors and settings to make Sleeve at home on your desktop.</p>
          <img style={{ paddingLeft: '20%' }} src="./images/pics.png" alt="Default" className="image" /><br></br>
          </div>

          <div className='div3'>
          <div className='div3-1'>
            
           <div className='part1'>
           <h1>Artwork</h1>
            <p className='para1'>Scale artwork all the way up or all the way down. Round the corners or leave them square.
            Choose shadow and lighting effects to bring your album artwork to life.
            Or hide it completely.</p>
           </div>

           <div className='part1'>
            <h1 className='para1'>Typography</h1>
            <p>Pick the track info you want to display, and then exactly how to display it.
            Choose the fonts, weights, sizes, and transparency to use for each line, along with customizing color and shadow.</p>
           </div>

          <div className='part1'>
            <h1>Interface</h1>
            <p>Customize the layout, alignment and position to fit your setup.
            Show and hide playback controls. Add a backdrop layer and customize it.</p>
          </div>

          </div>
          <div className='div3-2'>
            <div className='scroller'><img src="./images/scrollpic1.png" alt="Default" className="image" /></div><br></br><br></br><br></br><br></br>
            <div className='scroller'><img src="./images/scrollpic2.png" alt="Default" className="image" /></div><br></br><br></br>
            <div className='scroller'><img src="./images/scroll pic3.png" alt="Default" className="image" /></div><br></br><br></br><br></br><br></br>
            <div className='scroller'><img src="./images/scrollpic4.png" alt="Default" className="image" /></div><br></br><br></br>
          </div>
          </div>
          <div className='bottom'>
              <h1>  Like, Scrobble.</h1>
          </div>
        </div>
    </div>
  )
}

export default Portion3